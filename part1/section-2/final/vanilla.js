const pEle = document.querySelector(".p-text");
const inputEle = document.querySelector(".input-text");

let inputText = "";
inputEle.addEventListener("input", (event) => {
  // pEle.textContent = event.target.value;
  inputText = event.target.value;
});

inputEle.addEventListener("blur", (_event) => {
  pEle.textContent = inputText;
});
