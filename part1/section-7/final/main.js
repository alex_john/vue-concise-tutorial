// ["Vue", "React", "Nuxt", "NextJS", "Node", "NestJS", "React Native", "Electron", "Rust"]

const app = {
  data() {
    return {
      todoArr: [
        { item: "Vue", isComplete: false },
        { item: "React", isComplete: false },
      ],
      curTodo: "",
    };
  },

  methods: {
    addTodo() {
      if (!this.curTodo.trim().length) {
        alert("The input is empty");
        return;
      }

      this.todoArr.push({ item: this.curTodo, isComplete: false });
      this.curTodo = "";
    },

    removeTodo(removeTodo) {
      this.todoArr = this.todoArr.filter((todo) => todo !== removeTodo);
    },

    completeItem(item) {
      const index = this.todoArr.findIndex((todo) => todo.item === item);
      this.todoArr[index] = { ...this.todoArr[index], isComplete: true };
    },
  },
};

Vue.createApp(app).mount("#app");
