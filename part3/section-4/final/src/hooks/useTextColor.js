import { ref } from "vue";

export const useTextColor = () => {
  const textColor = ref("");
  const changeTextColor = (event) => {
    textColor.value = event.target.value;
  };

  return { textColor, changeTextColor };
};
