import getNews from "./getNews";

export async function getSportNews() {
  const data = await getNews(534);
  return data;
}
