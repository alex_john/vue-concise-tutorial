import getNews from "./getNews";

export async function getHotNews() {
  const data = await getNews(532);
  return data;
}
