export default async function (typeId) {
  try {
    const res = await fetch(
      `https://www.mxnzp.com/api/news/list/v2?typeId=${typeId}&page=1&app_id=pkjtupqkluvjslp9&app_secret=CbLDCfPviLfK8kkg8JL9EPpIQsUGilwd`
    );
    const data = await res.json();

    return data.data;
  } catch (error) {
    console.log(error.message);
    throw new Error(error.message);
  }
}
