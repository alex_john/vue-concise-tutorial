export async function getHowNews() {
  try {
    const res = await fetch(
      "https://www.mxnzp.com/api/news/list/v2?typeId=532&page=1&app_id=APP_ID&app_secret=APP_SECRET"
    );
    const data = await res.json();
    console.log(data.data);

    return data.data;
  } catch (error) {
    console.log(error.message);
    throw new Error(error.message);
  }
}
