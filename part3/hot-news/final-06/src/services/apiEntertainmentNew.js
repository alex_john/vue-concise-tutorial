import getNews from "./getNews";

export async function getEntertainmentNews() {
  const data = await getNews(533);
  return data;
}
