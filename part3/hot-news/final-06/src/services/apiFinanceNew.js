import getNews from "./getNews";

export async function getFinanceNews() {
  const data = await getNews(535);
  return data;
}
