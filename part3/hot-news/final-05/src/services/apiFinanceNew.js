export async function getFinanceNews() {
  try {
    const res = await fetch(
      "https://www.mxnzp.com/api/news/list/v2?typeId=535&page=1&app_id=APP_ID&app_secret=APP_SECRET"
    );
    const data = await res.json();

    return data.data;
  } catch (error) {
    console.log(error.message);
    throw new Error(error.message);
  }
}
